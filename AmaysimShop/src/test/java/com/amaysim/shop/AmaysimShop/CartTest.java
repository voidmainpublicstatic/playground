package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class CartTest extends TestCase {

	Map<String, PriceRule> priceRules = new HashMap<String, PriceRule>();
	
	public CartTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( CartTest.class );
    }

    @Override
	protected void setUp() throws Exception {
		super.setUp();
		
    	priceRules.put(ProductConstants.UL_SMALL, new PriceRuleThreeForTwo());
    	priceRules.put(ProductConstants.UL_MEDIUM, new PriceRuleFreeOneGb());
    	priceRules.put(ProductConstants.UL_LARGE, new PriceRuleMoreThanThree());
	}

	
    public void testScenario1()
    {
    	BigDecimal expectedResult = new BigDecimal("94.70");
    	
    	Cart cart = ShoppingCart.create(priceRules);
    	Product small = new Product(ProductConstants.UL_SMALL, "Unlimited 1GB", ProductConstants.UL_SMALL_PRICE);
    	Product large = new Product(ProductConstants.UL_LARGE, "Unlimited 5GB", ProductConstants.UL_LARGE_PRICE);
    	
    	Item smallItem = new Item(small, 3);
    	Item largeItem = new Item(large, 1);
    	
    	cart.addItem(smallItem);
    	cart.addItem(largeItem);
    	
    	assertEquals(expectedResult, cart.total());
    	
    	System.out.println("Test Scenario 1 - \n" + cart.items());
    }
    
    public void testScenario2()
    {
    	BigDecimal expectedResult = new BigDecimal("209.40");
    	
    	Cart cart = ShoppingCart.create(priceRules);
    	Product small = new Product(ProductConstants.UL_SMALL, "Unlimited 1GB", ProductConstants.UL_SMALL_PRICE);
    	Product large = new Product(ProductConstants.UL_LARGE, "Unlimited 5GB", ProductConstants.UL_LARGE_PRICE);
    	
    	Item smallItem = new Item(small, 2);
    	Item largeItem = new Item(large, 4);
    	
    	cart.addItem(smallItem);
    	cart.addItem(largeItem);
    	
    	assertEquals(expectedResult, cart.total());
    	
    	System.out.println("Test Scenario 2 - \n" + cart.items());
    }
    
    public void testScenario3()
    {
    	BigDecimal expectedResult = new BigDecimal("84.70");
    	
    	Cart cart = ShoppingCart.create(priceRules);
    	Product small = new Product(ProductConstants.UL_SMALL, "Unlimited 1GB", ProductConstants.UL_SMALL_PRICE);
    	Product medium = new Product(ProductConstants.UL_MEDIUM, "Unlimited 2GB", ProductConstants.UL_MEDIUM_PRICE);
    	
    	Item smallItem = new Item(small, 1);
    	Item mediumItem = new Item(medium, 2);
    	
    	cart.addItem(smallItem);
    	cart.addItem(mediumItem);
    	
    	assertEquals(expectedResult, cart.total());
    	
    	System.out.println("Test Scenario 3 - \n" + cart.items());
    }
    
    public void testScenario4()
    {
    	BigDecimal expectedResult = new BigDecimal("31.32");
    	String promoCode = "I<3AMAYSIM";
    	
    	Cart cart = ShoppingCart.create(priceRules);
    	Product small = new Product(ProductConstants.UL_SMALL, "Unlimited 1GB", ProductConstants.UL_SMALL_PRICE);
    	Product oneGb = new Product(ProductConstants.ONE_GB, "1 GB Data-pack", ProductConstants.ONE_GB_PRICE);
    	
    	Item smallItem = new Item(small, 1);
    	Item oneGbItem = new Item(oneGb, 1);
    	
    	cart.addItem(smallItem);
    	cart.addItem(oneGbItem, promoCode);
    	
    	assertEquals(expectedResult, cart.total());
    	
    	System.out.println("Test Scenario 4 - \n" + cart.items());
    }
}
