package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;

public class PriceRuleThreeForTwo extends PriceRule {
	
	private final int DIVISIBLE_BY = 3;

	@Override
	BigDecimal applyPriceRule(Cart cart, Item item) {
		BigDecimal total = new BigDecimal(0);
		
		int divisibleByThree = item.getQuantity() / DIVISIBLE_BY;
		//int remainder = item.getQuantity() % 3;
		
		if (divisibleByThree < 1) {
			total = item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity()));
		} else {
			int newQuantity = item.getQuantity() - divisibleByThree;
			total = item.getProduct().getPrice().multiply(new BigDecimal(newQuantity));
		}
		
		return total;
	}

}
