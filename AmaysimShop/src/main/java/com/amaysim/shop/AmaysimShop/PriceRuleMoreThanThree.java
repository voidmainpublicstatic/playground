package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;

public class PriceRuleMoreThanThree extends PriceRule {
	
	private final BigDecimal priceForMoreThanThree = new BigDecimal(39.90);

	@Override
	BigDecimal applyPriceRule(Cart cart, Item item) {
		BigDecimal total = new BigDecimal(0);
		
		if (item.getQuantity() > 3) {
			total = priceForMoreThanThree.multiply(new BigDecimal(item.getQuantity()));
		} else {
			total = item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity()));
		}
		
		return total;
	}
}
