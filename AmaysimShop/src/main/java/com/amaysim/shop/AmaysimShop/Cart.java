package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Cart {

	private String promoCode;
	private Map<String, Item> items = new HashMap<String, Item>();
	private Map<String, PriceRule> priceRules;
	
	public Cart(Map<String, PriceRule> priceRules){
		this.priceRules = priceRules;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public Map<String, Item> getItems() {
		return items;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public void setItems(Map<String, Item> items) {
		this.items = items;
	}
	
	public void addItem(Item newItem) {
		String productCode = newItem.getProduct().getCode();
		
		if (null != items.get(productCode)) {
			Item item = items.get(productCode);
			item.setQuantity(item.getQuantity() + newItem.getQuantity());
			
			if (ProductConstants.UL_MEDIUM.equalsIgnoreCase(productCode)) {
				Item freeItem = items.get(ProductConstants.ONE_GB);
				freeItem.setQuantity(freeItem.getQuantity() + newItem.getQuantity());
			}
		} else {
			this.getItems().put(productCode, newItem);
			
			if (ProductConstants.UL_MEDIUM.equalsIgnoreCase(productCode)) {
				Item freeItem = new Item(new Product(ProductConstants.ONE_GB, "1 GB Data-pack", ProductConstants.ONE_GB_PRICE), newItem.getQuantity());
				this.getItems().put(ProductConstants.ONE_GB, freeItem);
			}
		}
	}

	public void addItem(Item newItem, String promoCode) {
		addItem(newItem);
		this.setPromoCode(promoCode);
	}
	
	public BigDecimal total(){
		BigDecimal total = new BigDecimal(0);
		
		Set<String> keys = this.getItems().keySet();
		Iterator<String> productCodes = keys.iterator();
		
		while (productCodes.hasNext()) {
			String productCode = productCodes.next();
			Item item = this.getItems().get(productCode);
			
			if (null != this.priceRules.get(productCode)) {
				PriceRule priceRule = this.priceRules.get(productCode);
				total = total.add(priceRule.applyPriceRule(this, item));
			} else {
				total = total.add(item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity())));
			}
		}
		
		if (null != this.getPromoCode() && ProductConstants.PROMO_CODE.equals(this.getPromoCode().toUpperCase())) {
			BigDecimal discount = total.multiply(ProductConstants.DISCOUNT);
			total = total.subtract(discount);
		}
		
		return total.setScale(2, RoundingMode.UP);
	}
	
	public String items() {
		StringBuilder sb = new StringBuilder();
		
		/*Item mediumItem = this.getItems().get(ProductConstants.UL_MEDIUM);
		
		if (null != mediumItem) {
			PriceRule rule = new PriceRuleFreeOneGb();
			rule.applyPriceRule(this, mediumItem);
		}*/
		
		Set<String> keys = this.getItems().keySet();
		Iterator<String> productCodes = keys.iterator();
		
		while (productCodes.hasNext()) {
			String productCode = productCodes.next();
			Item item = this.getItems().get(productCode);
			
			sb.append(item.getQuantity());
			sb.append(" X ");
			sb.append(item.getProduct().getName());
			sb.append("\n");
		}
		
		return sb.toString();
	}
}
