package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;

public class ProductConstants {

	public final static String UL_SMALL = "ult_small";
	public final static String UL_MEDIUM = "ult_medium";
	public final static String UL_LARGE = "ult_large";
	public final static String ONE_GB = "1gb";
	
	public final static String PROMO_CODE = "I<3AMAYSIM";
	
	public final static BigDecimal UL_SMALL_PRICE = new BigDecimal("24.90");
	public final static BigDecimal UL_MEDIUM_PRICE = new BigDecimal("29.90");
	public final static BigDecimal UL_LARGE_PRICE = new BigDecimal("44.90");
	public final static BigDecimal ONE_GB_PRICE = new BigDecimal("9.90");
	public final static BigDecimal DISCOUNT = new BigDecimal("0.1");
}
