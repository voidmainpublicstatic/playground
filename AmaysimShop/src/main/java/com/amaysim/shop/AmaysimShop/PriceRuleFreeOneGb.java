package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;

public class PriceRuleFreeOneGb extends PriceRule {

	@Override
	BigDecimal applyPriceRule(Cart cart, Item item) {
		BigDecimal total = new BigDecimal(0);
		
		total = item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity()));
		total = total.subtract((ProductConstants.ONE_GB_PRICE.multiply(new BigDecimal(item.getQuantity()))));
		
		return total;
	}
}
