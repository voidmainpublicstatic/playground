package com.amaysim.shop.AmaysimShop;

import java.math.BigDecimal;

public abstract class PriceRule {

	abstract BigDecimal applyPriceRule(Cart cart, Item item);
}
