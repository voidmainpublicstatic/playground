package com.amaysim.shop.AmaysimShop;

import java.util.Map;

public class ShoppingCart {

	private ShoppingCart(){
		
	}
	
	public static Cart create(Map<String, PriceRule> priceRules) {
		return new Cart(priceRules);
	}
}
