package com.amaysim.shop.AmaysimShop;

public class Item {

	private int quantity;
	private Product product;
	//private Product freebies;
	
	public Item (Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public Product getProduct() {
		return product;
	}

	/*public Product getFreebies() {
		return freebies;
	}*/

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	/*public void setFreebies(Product freebies) {
		this.freebies = freebies;
	}*/

}
